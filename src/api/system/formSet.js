import request from '@/utils/request'

export function loadDataUrl() {
  return 'form-service/api/formSetMng/loadData'
}

export function findFormByCpyCodeUrl() {
  return 'form-service/api/formSetMng/findFormByCpyCode'
}

export function submitUrl() {
  return 'form-service/api/formSetMng/doSubmit'
}

export function deleteUrl() {
  return 'form-service/api/formSetMng/doDelete'
}

export function destoryUrl() {
  return 'form-service/api/formSetMng/doDestory'
}

export function loadPrivFieldDataUrl() {
  return 'form-service/api/formSetMng/loadPrivFieldData'
}

export function loadHistoryDataUrl() {
  return 'form-service/api/formSetMng/loadHistoryData'
}

export function getFormContent(mainId) {
  return request({
    url: 'form-service/api/formSetMng/getFormContent',
    method: 'get',
    params: {
      mainId
    }
  })
}

export function getExtendFormInfo(moduleBusClass) {
  return request({
    url: 'form-service/api/formSetMng/getExtendFormInfo',
    method: 'get',
    params: {
      moduleBusClass
    }
  })
}

export function getFormApplyTreeData() {
  return request({
    url: 'form-service/api/formSetMng/getFormApplyTreeData',
    method: 'get'
  })
}

export function viewMainData(data) {
  return request({
    url: 'form-service/api/formSetMng/toView',
    method: 'post',
    data
  })
}

export function toAdd(data) {
  return request({
    url: 'form-service/api/formSetMng/toAdd',
    method: 'post',
    data
  })
}

export function toChange(data) {
  return request({
    url: 'form-service/api/formSetMng/toChange',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'form-service/api/formSetMng/toCopy',
    method: 'post',
    data
  })
}

export function doSaveFormDesign(data) {
  return request({
    url: 'form-service/api/formSetMng/doSaveFormDesign',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'form-service/api/formSetMng/doSave',
    method: 'post',
    data
  })
}

export function doSaveFieldPriv(data) {
  return request({
    url: 'form-service/api/formSetMng/doSaveFieldPriv',
    method: 'post',
    data
  })
}

export function getFieldPrivInfo(fieldId) {
  return request({
    url: 'form-service/api/formSetMng/getFieldPrivInfo',
    method: 'get',
    params: {
      fieldId: fieldId
    }
  })
}

export function del(data) {
  return request({
    url: 'form-service/api/formSetMng/doDelete',
    method: 'post',
    data
  })
}

export function getFeildDetailInfo(mainId) {
  return request({
    url: 'form-service/api/formSetMng/getFeildDetailInfo',
    method: 'get',
    params: {
      mainId: mainId
    }
  })
}

export function getFeildPrivDetailInfo(mainId) {
  return request({
    url: 'form-service/api/formSetMng/getFeildPrivDetailInfo',
    method: 'get',
    params: {
      mainId: mainId
    }
  })
}
