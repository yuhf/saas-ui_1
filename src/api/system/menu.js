import request from '@/utils/request'

export function buildMenus() {
  return request({
    url: 'admin-service/api/menuMng/build',
    method: 'get'
  })
}

export function getAllExtendMenu() {
  return request({
    url: 'admin-service/api/menuMng/getAllExtendMenu',
    method: 'get'
  })
}

export function getAllWorkflowMenusTree() {
  return request({
    url: 'admin-service/api/menuMng/getAllWorkflowMenusTree',
    method: 'get'
  })
}

export function getAllMenusTree() {
  return request({
    url: 'admin-service/api/menuMng/getAllMenusTree',
    method: 'get'
  })
}

export function getMenuLanByMenuId(menuId) {
  return request({
    url: 'admin-service/api/menuMng/getMenuLanByMenuId?menuId=' + menuId,
    method: 'get'
  })
}

export function loadDataUrl() {
  return 'admin-service/api/menuMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/menuMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/menuMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/menuMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/menuMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/menuMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/menuMng/doDelete',
    method: 'post',
    data
  })
}
