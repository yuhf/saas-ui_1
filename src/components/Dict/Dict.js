import Vue from 'vue'
import { getDictByType } from '@/api/system/dataDict'

export default class Dict {
  constructor(dicts) {
    this.dicts = dicts
  }

  async init(dictTypes, completeCallback) {
    if (dictTypes === undefined) {
      throw new Error('need Dict types')
    }
    const ps = []
    let dataStr = ''
    dictTypes.forEach(dictType => {
      // Vue.set(this.dict.dict, n, {})
      // Vue.set(this.dict.label, n, {})
      if (this.dicts[dictType] && this.dicts[dictType].length > 0) {
        return
      }
      Vue.set(this.dicts, dictType, [])
      dataStr = dictType + ',' + dataStr
    })
    ps.push(getDictByType(dataStr).then(res => {
      const rtnMap = res.data
      const dictKeys = Object.keys(rtnMap)
      for (const dictKey of dictKeys) {
        this.dicts[dictKey].splice(0, 0, ...rtnMap[dictKey])
      }
      /* data.content.forEach(d => {
        Vue.set(this.dict.dict[n], d.value, d)
        Vue.set(this.dict.label[n], d.value, d.label)
      })*/
    }))
    await Promise.all(ps)
    completeCallback()
  }
}
