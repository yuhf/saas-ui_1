import Config from '@/config'
import variables from '@/styles/element-variables.scss'
import Cookies from 'js-cookie'
import { parseBoolean } from '@/utils'

const settings = {
  state: {
    showRightPanel: false,
    tagsView: Cookies.get('tagsView') === undefined ? Config.tagsView : parseBoolean(Cookies.get('tagsView')),
    fixedHeader: Cookies.get('fixedHeader') === undefined ? Config.fixedHeader : parseBoolean(Cookies.get('fixedHeader')),
    sidebarLogo: Cookies.get('sidebarLogo') === undefined ? Config.sidebarLogo : parseBoolean(Cookies.get('sidebarLogo')),
    theme: variables.theme,
    settingBtn: Cookies.get('settingBtn') === undefined ? Config.settingBtn : parseBoolean(Cookies.get('settingBtn')),
    uniqueOpened: Cookies.get('uniqueOpened') === undefined ? Config.uniqueOpened : parseBoolean(Cookies.get('uniqueOpened')),
    openMenuBlank: Cookies.get('openMenuBlank') === undefined ? Config.openMenuBlank : parseBoolean(Cookies.get('openMenuBlank'))
  },
  mutations: {
    CHANGE_SETTING: (state, { key, value }) => {
      if (key === 'showRightPanel') {
        state[key] = value
        return
      }
      if (!state.hasOwnProperty(key) || !state.showRightPanel) {
        return
      }
      state[key] = value
      Cookies.set(key, value, { expires: Config.passCookieExpires })
    }
  },
  actions: {
    changeSetting({ commit }, data) {
      commit('CHANGE_SETTING', data)
    }
  }
}
export default settings
