<template>
  <el-dialog :visible.sync="dialog" :modal="false" :disabled="!form.busPageInfo.editable" :close-on-click-modal="false" title="表单配置" width="70%" top="10px" append-to-body fullscreen>
    <el-row>
      <el-col>
        <el-row :gutter="15">
          <el-form ref="form" :model="form" :rules="rules" size="medium" label-width="100px">
            <el-col :span="12">
              <el-form-item label="表单名称" prop="busMainData.formName">
                <el-input :maxlength="100" :style="{width: '100%'}" v-model="form.busMainData.formName" placeholder="请输入表单名称" show-word-limit clearable />
              </el-form-item>
            </el-col>
            <el-col :span="12">
              <el-form-item label="表单类型" prop="busMainData.formType">
                <el-select v-model="form.busMainData.formType" :style="{width: '100%'}" placeholder="请选表单类型">
                  <el-option
                    v-for="(item) in dicts.form_type"
                    :key="item.id"
                    :label="item.dictName"
                    :value="item.dictCode"/>
                </el-select>
              </el-form-item>
            </el-col>
            <el-col v-if="form.busMainData.formType === 'OA'" :span="12">
              <el-form-item label="OA菜单名称" prop="busMainData.oaMenuName">
                <el-input :maxlength="100" :style="{width: '100%'}" v-model="form.busMainData.oaMenuName" placeholder="请输入OA菜单名称" show-word-limit clearable/>
              </el-form-item>
            </el-col>
            <el-col v-if="form.busMainData.formType === 'EXT'" :span="12">
              <el-form-item label="扩展模块" prop="busMainData.moduleBusClass">
                <el-select v-model="form.busMainData.moduleBusClass" :style="{width: '100%'}" placeholder="请选扩展模块">
                  <el-option
                    v-for="(item) in moduleMenuList"
                    :key="item.moduleBusClass"
                    :label="item.menuName"
                    :value="item.moduleBusClass"/>
                </el-select>
              </el-form-item>
            </el-col>
          </el-form>
        </el-row>
      </el-col>
    </el-row>
    <!-- <el-row>
      <el-col>
        <tinymce-editor
          v-if="dialog"
          ref="editor"
          v-model="form.formSetContentVO.formContent"
        />
      </el-col>
    </el-row>-->
    <div v-if="form.busPageInfo.editable" slot="footer" class="dialog-footer">
      <el-button type="text" @click="cancel">取消</el-button>
      <el-button v-permission="['SYS_ADMIN','COMMON_ADMIN','FORM_SET_ALL','FORM_SET_CHANGE','FORM_SET_CREATE']" :loading="loading" type="primary" @click="doSave">暂存</el-button>
      <!-- <el-button v-permission="['SYS_ADMIN','COMMON_ADMIN','FORM_SET_ALL','FORM_SET_SUBMIT']" :loading="loading" type="primary" @click="doSubmit">提交</el-button>-->
    </div>
  </el-dialog>
</template>

<script>
import { addOrEdit, viewMainData, toAdd, toChange, toCopy } from '@/api/system/formSet'
// import { getAllExtendMenu } from '@/api/system/menu'
// import TinymceEditor from '@/views/components/TinymceEdit' getFeildDetailInfo

export default {
  // components: { TinymceEditor },
  props: {
    sup_this: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      isAdd: true,
      loading: false,
      formLoading: false,
      dialog: false,
      moduleMenuList: [],
      form: {
        funProcess: '10',
        formSubmit: false,
        busPageInfo: {
          editable: true
        },
        formSetContentVO: {
          formContent: '',
          formHtml: ''
        },
        busMainData: {
          id: null,
          formName: '',
          formType: '',
          moduleBusClass: '',
          oaMenuName: '',
          enableFlag: 'Y'
        },
        busCommonState: {
        }
      },
      rules: {
        'busMainData.formCode': [
          { required: true, message: '请输入Form代码', trigger: 'blur' }
        ],
        'busMainData.formName': [
          { required: true, message: '请输入名称', trigger: 'blur' },
          { min: 2, max: 100, message: '长度在 2 到 100 个字符', trigger: 'blur' }
        ],
        'busMainData.oaMenuName': [
          { required: true, message: '请输入OA菜单名称', trigger: 'blur' },
          { min: 2, max: 100, message: '长度在 2 到 100 个字符', trigger: 'blur' }
        ]
      }
    }
  },
  methods: {
    cancel() {
      this.resetForm()
    },
    initDataOfBefore() {

    },
    viewMainData(mainId) {
      const data = { paramBean: { id: mainId }}
      viewMainData(data).then(res => {
        this.form = res.data
        if (this.form.busPageInfo.editable) {
          this.getFeildDetailInfo(mainId)
        }
      })
    },
    getFeildDetailInfo(mainId) {
      // 获取field定义设置
      /* getFeildDetailInfo(mainId).then(res => {
        const fieldData = res.data
        const _this = this
        _this.$refs.editor.fieldVOList = fieldData
      }) */
    },
    addData() {
      const data = { paramBean: { }}
      toAdd(data).then(res => {
        this.dialog = true
        this.form = res.data
      }).catch(err => {
        this.dialog = false
        console.log(err.response)
      })
    },
    copyData(mainId) {
      const data = { paramBean: { id: mainId }}
      toCopy(data).then(res => {
        this.dialog = true
        this.form = res.data
        this.getFeildDetailInfo(mainId)
      }).catch(err => {
        this.dialog = false
        console.log(err.response)
      })
    },
    changeData(busId) {
      const data = { paramBean: { id: busId }}
      toChange(data).then(res => {
        this.dialog = true
        this.form = res.data
        this.getFeildDetailInfo(this.form.busMainData.oldMainId)
      }).catch(err => {
        this.dialog = false
        console.log(err.response)
      })
    },
    doSubmit() {
      this.doSingleSave(true)
    },
    doSave() {
      this.doSingleSave(false)
    },
    doSingleSave(isSubmit) {
      this.$refs['form'].validate((valid) => {
        if (valid) {
          this.form.formSubmit = isSubmit
          // this.form.formSetFieldVOList = this.$refs.editor.getAllFormElement()
          this.loading = true
          if (this.isAdd) {
            this.doAdd()
          } else {
            this.doEdit()
          }
        }
      })
    },
    doAdd() {
      addOrEdit(this.form).then(res => {
        this.resetForm()
        this.loading = false
        this.sup_this.init()
      }).catch(err => {
        this.loading = false
        console.log(err)
      })
    },
    doEdit() {
      addOrEdit(this.form).then(res => {
        this.resetForm()
        this.loading = false
        this.sup_this.init()
      }).catch(err => {
        this.loading = false
        console.log(err)
      })
    },
    resetForm() {
      this.dialog = false
      this.$refs['form'].resetFields()
    }
  }
}
</script>
